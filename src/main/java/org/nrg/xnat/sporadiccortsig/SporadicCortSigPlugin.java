package org.nrg.xnat.sporadiccortsig;

import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(value = "nrg_plugin_sporadic", name = "XNAT 1.7 Sporadic AD Cortical Signature Plugin", description = "This is the XNAT 1.7 Sporadic AD Cortical Signature Plugin.")
@ComponentScan({"org.nrg.xnat.workflow.listeners"})
public class SporadicCortSigPlugin {
}